import React, { Component } from "react";
import {shoeArr} from "./data_shoeShop";
import ItemShoe from "./ItemShoe";
import ListShoeRd from "./ListShoeRd";
import TableGioHang from "./TableGioHang";


export default class Ex_ShoeShopRd_1 extends Component {
  state = {
    shoeArr: shoeArr,
    gioHang: [],
  };
  handleAddToCard = (sp) => {

    let index = this.state.gioHang.findIndex((item) => {
      return item.id == sp.id;
    });
    let cloneGioHang = [...this.state.gioHang];
    if (index == -1) {
      let newSp = { ...sp, soLuong: 1 };
      cloneGioHang.push(newSp);
      
    } else {
      cloneGioHang[index].soLuong++;
 
    }
    this.setState({
      gioHang: cloneGioHang,
    });
  };

  handleRemoveShoe = (idShoe) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idShoe;
    });
    if (index !== -1) {
      let cloneGioHang = [...this.state.gioHang];
      cloneGioHang.splice(index, 1);
      this.setState({ gioHang: cloneGioHang });
    }
  };

  handleChangeQuantity = (idShoe, step) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idShoe;
    });
    let cloneGioiHang = [...this.state.gioHang];
    // 5+1=6
    // 5 + -1=4
    cloneGioiHang[index].soLuong = cloneGioiHang[index].soLuong + step;
    if (cloneGioiHang[index].soLuong == 0) {
      cloneGioiHang.splice(index, 1);
    }

    this.setState({ gioHang: cloneGioiHang }, () => {
      console.log(this.state.gioHang.length);
    });
  };
  render() {
    return (
      <div className="container py-5">
        {/* {this.state.gioHang.length > 0 && ( */}
          <TableGioHang
            handleRemoveShoe={this.handleRemoveShoe}
            gioHang={this.state.gioHang}
            handleChangeQuantity={this.handleChangeQuantity}
          />
        {/* )} */}

        {/* <div className="row">{this.renderShoes()}</div> */}
        <ListShoeRd/>
      </div>
    );
  }
}
