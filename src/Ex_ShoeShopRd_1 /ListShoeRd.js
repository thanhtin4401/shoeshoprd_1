import React, { Component } from 'react'
import ItemShoe from './ItemShoe'
import { connect } from "react-redux";

class ListShoeRd extends Component {
    renderShoes =() => {
        return this.props.shoeArr.map((item)=>{
            return (
                <ItemShoe
                handleAddToCard={() => {
                    
                }}
                shoeData={item}
                key={item.id}
                />
            )
        })
    }   
    render() {
        return (
            <div className="row">
                {this.renderShoes()}
            </div>
        )
    }
}

let mapStateToProp=(state)=>{
    return{
        shoeArr:state.shoeShopReducer.shoeArr,
    };
}
// lấy dữ liệu về từ thằng redux
export default connect(mapStateToProp)(ListShoeRd)