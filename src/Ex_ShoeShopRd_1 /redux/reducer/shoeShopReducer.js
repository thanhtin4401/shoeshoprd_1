import { shoeArr } from "../../data_shoeShop";
import { ADD_TO_CART } from "../constant/shoeShopeConstant";

let initialState={
    shoeArr: shoeArr,
    gioHang: [],
}
// action đổi lại dùng cú pháp es6 để bóc tách dữ liệu nhanh
export let shoeShopReducer=(state=initialState,{type,payload})=>{
    // thay vì action.style
    switch(type){
        case ADD_TO_CART:{
            let index = state.gioHang.findIndex((item) => {
                return item.id == payload.id;
              });
              let cloneGioHang = [...state.gioHang];
              if (index == -1) {
                let newSp = { ...payload, soLuong: 1 };
                cloneGioHang.push(newSp);
                // th1
              } else {
                cloneGioHang[index].soLuong++;
                // th2
              }
              state.gioHang=cloneGioHang;
              return{...state}
              
        }
        default:
            return state;
    }
}