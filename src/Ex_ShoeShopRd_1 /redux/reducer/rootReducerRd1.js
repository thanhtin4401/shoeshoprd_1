import { combineReducers } from "redux"
import { shoeShopReducer } from "./shoeShopReducer";

export let rootReducer_shoeShopRd1=combineReducers({
    // kéo qua từ thằng reducer vì key và value giống nhau có thể giảm tải
    shoeShopReducer
});
